#!/usr/bin/env python3

# bigram-quoter-python - Python edition of Joshua Brockschmidt's bigram_quoter
# Copyright (C) 2015 Robert Cochran
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import sys
import re
import pprint
import itertools
import random
import json
import os

bigram_words = {"!start!" : {}}

if len(sys.argv) == 1:
    print("Need to supply data file for analysis")
    sys.exit(-1)

# Check modify times - use a saved chain file if possible
try:
    file_stat = os.stat(sys.argv[1])
except FileNotFoundError:
    # Or, in this case, bail because there is no file period
    print("Could not find file '{}'".format(sys.argv[1]))
    sys.exit(-1)

try:
    cache_stat = os.stat(sys.argv[1] + ".bigram")
    # Use original file if newer
    use_cache = False if file_stat.st_mtime > cache_stat.st_mtime else True
except FileNotFoundError:
    # No cache file, no choice
    use_cache = False

if not use_cache:
    # Initialize words array with dummy element for zip processing
    words = [None]

    with open(sys.argv[1], "r") as words_file:
        for line in words_file:
            # Strip words of newlines
            line_words = [re.sub("\n", "", x) for x in line.split(" ")]

            for i, word in enumerate(line_words):
                # Check if the word ends a sentence and is not lone punctuation
                try:
                    if word not in ('.', '!', '?') and word[-1] in ('.', '!', '?'):
                        # Insert ending punctuation as its own 'word'
                        line_words.insert(i + 1, word[-1])
                        # Remove the punctuation from the original word
                        line_words[i] = word[:-1]
                except IndexError:
                    # Blake line, ignore
                    break

            words.extend(line_words)

    for current_word, previous_word in zip(words[1:], words):
        if current_word not in bigram_words:
            bigram_words[current_word] = {}

        # First word in the sentence
        if previous_word in (None, '.', '!', '?'):
            # Set previous 'word' to sentence start marker
            previous_word = "!start!"

        if current_word not in bigram_words[previous_word]:
            bigram_words[previous_word][current_word] = 1;
        else:
            bigram_words[previous_word][current_word] += 1;

    # Finished building bigram structure, save it to disk for next time
    with open(sys.argv[1] + ".bigram", "w") as cache_file:
        cache_file.write(json.dumps(bigram_words))
else:
    with open(sys.argv[1] + ".bigram", "r") as cache_file:
        bigram_words = json.loads(cache_file.read())

# Build a bigram
bigram = ""
current_word = "!start!"

while len(bigram_words[current_word]) > 0:
    # Look at the words that follow the current word,
    # taking frequency into account
    possible_words = []

    for word, frequency in bigram_words[current_word].items():
        possible_words.extend(itertools.repeat(word, frequency))

    next_word = random.choice(possible_words)

    bigram += next_word + " "

    current_word = next_word

# Fix the sentence, removing the space before the ending punctuation
bigram = bigram[:-3] + bigram[-2]

print(bigram)
